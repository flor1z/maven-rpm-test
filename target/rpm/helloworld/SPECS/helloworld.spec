%define __jar_repack 0
Name: helloworld
Version: 1.0
Release: 1.0.0
Summary: helloworld
License: (c) null
Group: Development/Tools
autoprov: yes
autoreq: yes
BuildArch: noarch
BuildRoot: /usr/etc/maven-rpm-test/helloworld/target/rpm/helloworld/buildroot

%description

%install

if [ -d $RPM_BUILD_ROOT ];
then
  mv /usr/etc/maven-rpm-test/helloworld/target/rpm/helloworld/tmp-buildroot/* $RPM_BUILD_ROOT
else
  mv /usr/etc/maven-rpm-test/helloworld/target/rpm/helloworld/tmp-buildroot $RPM_BUILD_ROOT
fi

%files

%attr(555,root,-) "/usr/etc/hello"
%attr(644,root,-)  "/lib/systemd/system/hello.service"

%pre
echo "installing helloworld now"

%post
systemctl daemon-reload
