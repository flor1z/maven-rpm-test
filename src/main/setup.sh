#!/bin/bash

copy hello-world /usr/etc/hello-world
chmod +x /usr/etc/hello-world
copy hello.service /etc/systemd/system/hello.service
systemctl daemon-reload
systemctl enable hello.service